import baseRoutes from '@common/router/routes';
import { t } from '@/locales';
// import Home from '../views/index.vue';
import RoleAuthorization from '@/views/role-authorization/index.vue';
import DataAuthority from '@/views/data-authority/index.vue';
import SaasRbac from '@/views/saas-role-authorization/index.vue';
import ThreeMemberManagement from '@/views/three-member-management/index.vue';
/**
 * 基础路由
 * 如果有业务需要，可以自行调整
 * 固定路由：Home(/),Login(/login)
 */
const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: '/roleAuthority',
  },
  {
    path: '/roleAuthority',
    menu: true,
    text: t('roleAuthority'),
    name: 'RoleAuthorization',
    component: RoleAuthorization,
  },
  {
    path: '/threeMemberManagement',
    menu: true,
    text: t('threeMemberManagement'),
    name: 'ThreeMemberManagement',
    component: ThreeMemberManagement,
  },
  {
    path: '/dataAuthority',
    menu: true,
    text: t('dataAuthority'),
    name: 'DataAuthority',
    component: DataAuthority,
  },
  {
    path: '/saasRoleAuthority',
    menu: true,
    text: t('saasRoleAuthority'),
    name: 'SaasRoleAuthorization',
    component: SaasRbac,
  },
  ...baseRoutes,
];
export default routes;
// const routes = [
//   {
//     path: '/',
//     name: 'Home',
//     text: t('menu.home'),
//     menu: true,
//     component: Home,
//   },
//   ...baseRoutes,
// ];
// export default routes;

/**
 * 自定义路由History
 * @returns History
 */
// export function createRouterHistory() {
//   return createMemoryHistory();
// }

/**
 * 自定义路由守卫
 */
// export function createRouterGuard(router) {
//   // 业务逻辑
// }

/**
 * 路由白名单
 */
//  export const whiteRoutes = [];
