import { h } from 'vue';
import AasaRoleAuthorization from '../views/saas-role-authorization/layout.vue';
import { Modal } from '@jecloud/ui';

export function showSaasRbacWin({ productData }, fn) {
  Modal.window({
    title: '角色权限',
    width: '100%',
    height: '100%',
    bodyStyle: 'padding: 0 10px 10px 10px;',
    headerStyle: 'height:50px;',
    content() {
      return h(AasaRoleAuthorization, { productData });
    },
    //关闭方法
    onClose(model) {
      fn && fn();
    },
  });
}
