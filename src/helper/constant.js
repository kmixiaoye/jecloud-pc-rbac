/**
 * 系统常量配置文件，命名规则
 * 平台核心：JE_模块_变量名
 * 业务常量：业务(MENU)_模块_变量名
 */
// 快速授权list数据
export const JE_DATAAUTHORITY_ITEMLIST = [
  {
    title: '部门主管可见',
    info: '部门主管可见部门内所有数据',
    id: 'deptHeadQuery',
    value: '0',
    disabled: 'true',
  },
  {
    title: '本部门内可见',
    info: '同属一个部门的用户数据相互可见',
    id: 'myDeptQuery',
    value: '0',
    disabled: 'true',
  },
  {
    title: '部门主管可修改',
    info: '部门主管可修改部门内所有数据',
    id: 'deptHeadEdit',
    value: '0',
    disabled: 'true',
  },
  {
    title: '本部门内可修改',
    info: '同属一个部门的用户数据相互可修改',
    id: 'myDeptEdit',
    value: '0',
    disabled: 'true',
  },
  {
    title: '部门主管可删除',
    info: '部门主管可删除部门内所有数据',
    id: 'deptHeadDelete',
    value: '0',
    disabled: 'true',
  },
  {
    title: '本部门内可删除',
    info: '同属一个部门的用户数据相互可删除',
    id: 'myDeptDelete',
    value: '0',
    disabled: 'true',
  },
  {
    title: '直属领导可见',
    info: '直属领导可见下属录入数据',
    id: 'directLeaderQuery',
    value: '0',
    disabled: 'true',
  },
  {
    title: '部门间监管可见',
    info: '通过部门间监管关系过滤数据',
    id: 'deptMonitorDeptQuery',
    value: '0',
    disabled: 'true',
  },
  {
    title: '直属领导可修改',
    info: '直属领导可修改下属录入数据',
    id: 'directLeaderEdit',
    value: '0',
    disabled: 'true',
  },
  {
    title: '部门间监管可修改',
    info: '通过部门间监管关系修改数据',
    id: 'deptMonitorDeptEdit',
    value: '0',
    disabled: 'true',
  },
  {
    title: '直属领导可删除',
    info: '直属领导可删除下属录入数据',
    id: 'directLeaderDelete',
    value: '0',
    disabled: 'true',
  },
  {
    title: '部门间监管可删除',
    info: '通过部门间监管关系删除数据',
    id: 'deptMonitorDeptDelete',
    value: '0',
    disabled: 'true',
  },
  {
    title: '监管部门领导可见',
    info: '用户可看到自己监管部门的数据',
    id: 'monitorLeaderQuery',
    value: '0',
    disabled: 'true',
  },
  {
    title: '公司内可见',
    info: '公司内可见',
    id: 'myCompanyQuery',
    value: '0',
    disabled: 'true',
  },
  {
    title: '监管部门领导可修改',
    info: '用户修改到自己监管部门的数据',
    id: 'monitorLeaderEdit',
    value: '0',
    disabled: 'true',
  },
  {
    title: '公司内可修改',
    info: '公司内可修改',
    id: 'myCompanyEdit',
    value: '0',
    disabled: 'true',
  },
  {
    title: '监管部门领导可删除',
    info: '用户删除到自己监管部门的数据',
    id: 'monitorLeaderDelete',
    value: '0',
    disabled: 'true',
  },
  {
    title: '公司内可删除',
    info: '公司内可删除',
    id: 'myCompanyDelete',
    value: '0',
    disabled: 'true',
  },
  {
    title: '公司间监管可见',
    info: '通过公司间监管关系过滤数据',
    id: 'monitorCompanyQuery',
    value: '0',
    disabled: 'true',
  },
  {
    title: '集团内可见',
    info: '集团内可见',
    id: 'groupCompanyQuery',
    value: '0',
    disabled: 'true',
  },
  {
    title: '公司间监管可修改',
    info: '通过公司间监管关系修改数据',
    id: 'monitorCompanyEdit',
    value: '0',
    disabled: 'true',
  },
  {
    title: '集团内可修改',
    info: '集团内可修改',
    id: 'groupCompanyEdit',
    value: '0',
    disabled: 'true',
  },
  {
    title: '公司间监管可删除',
    info: '通过公司间监管关系删除数据',
    id: 'monitorCompanyDelete',
    value: '0',
    disabled: 'true',
  },
  {
    title: '集团内可删除',
    info: '集团内可删除',
    id: 'groupCompanyDelete',
    value: '0',
    disabled: 'true',
  },
  {
    title: '待审批可见',
    info: '数据通过流程流转到自己的时候才可见',
    id: 'approPreQuery',
    value: '0',
    disabled: 'true',
  },
  {
    title: '审批后可见',
    info: '用户审批过本条数据后一直可见',
    id: 'approAfterQuery',
    value: '0',
    disabled: 'true',
  },
];

// 快速授权tree选择数据
export const JE_DATAAUTHORITY_TREESELECTDATA = [
  {
    title: '人员控制',
    info: '选择任何人员可见及操作本功能数据',
    id: 'userControl',
    userControl: '',
    texts: '',
    disabled: 'true',
    ids: '',
    config: '',
  },
  {
    title: '角色控制',
    info: '选择任何角色可见及操作本功能数据',
    id: 'roleControl',
    roleControl: '',
    texts: '',
    ids: '',
    disabled: 'true',
    config: '',
  },
  {
    title: '部门控制',
    info: '选择任何部门可见及操作本功能数据',
    id: 'deptControl',
    deptControl: '',
    texts: '',
    TextTrackCueList: '',
    disabled: 'true',
    config: '',
    ids: '',
  },
  {
    title: '机构控制',
    info: '选择机构可见及操作本功能数据',
    id: 'orgControl',
    orgControl: '',
    texts: '',
    ids: '',
    disabled: 'true',
    config: '',
  },
];

// 角色sql授权的SQL选项
export const JE_DATAAUTHORITY_RSASQLOOPTIONS = [
  { label: '关闭角色SQL授权', value: '0' },
  { label: '打开角色SQL授权', value: '1' },
];

// 角色sql授权的授权选项
export const JE_DATAAUTHORITY_RSAAUTHORIZATIONOPTIONS = [
  { label: '使用角色授权', value: '0' },
  { label: '使用部门授权', value: '1' },
  { label: '使用机构授权', value: '2' },
];

// 快速授权的SQL选项
export const JE_DATAAUTHORITY_RASQLOOPTIONS = [
  { label: '关闭数据权限', value: '0' },
  { label: '打开数据权限', value: '1' },
];

// 快速授权payload数据
export const JE_DATAAUTHORITY_RAPAYLOADDATA = {
  roleControl: {
    configPerm: '',
    ids: '',
    texts: '',
  },
  deptControl: {
    configPerm: '',
    ids: '',
    texts: '',
  },
  orgControl: {
    configPerm: '',
    ids: '',
    texts: '',
  },
  userControl: {
    configPerm: '',
    ids: '',
    texts: '',
  },
  selectPermStr: '',
};

// 快速授权payload的对照组数据
export const JE_DATAAUTHORITY_RAPAYLOADMETADATA = {
  roleControl: {
    configPerm: '',
    ids: '',
    texts: '',
  },
  deptControl: {
    configPerm: '',
    ids: '',
    texts: '',
  },
  orgControl: {
    configPerm: '',
    ids: '',
    texts: '',
  },
  userControl: {
    configPerm: '',
    ids: '',
    texts: '',
  },
  selectPermStr: '',
};

// 快速授权config配置数据
export const JE_DATAAUTHORITY_CONFIGDATA = {
  userControl: {
    controlQuery: 'userControlQuery',
    controlEdit: 'userControlEdit',
    controlDelete: 'userControlDelete',
  },
  deptControl: {
    controlQuery: 'deptControlQuery',
    controlEdit: 'deptControlEdit',
    controlDelete: 'deptControlDelete',
  },
  orgControl: {
    controlQuery: 'orgControlQuery',
    controlEdit: 'orgControlEdit',
    controlDelete: 'orgControlDelete',
  },
  roleControl: {
    controlQuery: 'roleControlQuery',
    controlEdit: 'roleControlEdit',
    controlDelete: 'roleControlDelete',
  },
};

// 快速授权tree型选择器配置项
export const JE_DATAAUTHORITY_TREECONFIGDATA = {
  userControl: {
    userControlQuery: 'controlQuery',
    userControlEdit: 'controlEdit',
    userControlDelete: 'controlDelete',
  },
  deptControl: {
    deptControlQuery: 'controlQuery',
    deptControlEdit: 'controlEdit',
    deptControlDelete: 'controlDelete',
  },
  orgControl: {
    orgControlQuery: 'controlQuery',
    orgControlEdit: 'controlEdit',
    orgControlDelete: 'controlDelete',
  },
  roleControl: {
    roleControlQuery: 'controlQuery',
    roleControlEdit: 'controlEdit',
    roleControlDelete: 'controlDelete',
  },
};

// 字典授权的SQL选项
export const JE_DATAAUTHORITY_DASQLOOPTIONS = [
  { label: '关闭字典授权', value: '0' },
  { label: '打开字典授权', value: '1' },
];

// 字典授权的授权选项
export const JE_DATAAUTHORITY_DAAUTHORIZATIONOPTIONS = [
  { label: '使用角色授权', value: '0' },
  { label: '使用部门授权', value: '1' },
  { label: '使用机构授权', value: '2' },
];

// 字段授权SQL选项
export const JE_DATAAUTHORITY_AFSQLOPTIONS = [
  { label: '关闭字段授权', value: '0' },
  { label: '打开字段授权', value: '1' },
];

// 字段授权授权范围选项
export const JE_DATAAUTHORITY_AFAUTHORIZATIONOPTIONS = [
  { label: '使用角色授权', value: '0' },
  { label: '使用部门授权', value: '1' },
  { label: '使用机构授权', value: '2' },
];

// 字段授权控制范围选项
export const JE_DATAAUTHORITY_AFCTROPTIONS = [
  { label: '控制可见权限', value: '0' },
  { label: '控制编辑权限', value: '1' },
];

// 字段授权选择控制选项
export const JE_DATAAUTHORITY_AFCHANGECTROPTIONS = [
  { label: '选择默认是启用', value: '0' },
  { label: '选择默认是禁用', value: '1' },
];
